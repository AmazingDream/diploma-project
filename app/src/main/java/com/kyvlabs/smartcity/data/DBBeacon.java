package com.kyvlabs.smartcity.data;

import com.kyvlabs.smartcity.common.network.model.BeaconAdditionalContent;
import com.kyvlabs.smartcity.common.network.model.BeaconModel;

import java.util.List;

//Beacon entity used to store and load from database/
public class DBBeacon {
    private BeaconIds ids;
    private String title;
    private String picture;
    private String description;
    private String link;
    //when we can show this beacon next time
    private long nextShowTime;
    private long nextUpdateTime;
    private String groupToBind;
    private String groupName;
    private String absoluteMapFolderUrl;
    private int mapWidth;
    private int mapHeight;
    private double beaconPinX;
    private double beaconPinY;
    private int nextShownContent;
    private int favorite = 0;
    private List<BeaconAdditionalContent> additionalContent;

    public DBBeacon() {
    }

    public DBBeacon(BeaconModel beaconModel) {
        ids = new BeaconIds(beaconModel.getUuid(), beaconModel.getMajor(), beaconModel.getMinor());
        title = beaconModel.getTitle();
        picture = beaconModel.getAbsolutePicture();
        description = beaconModel.getDescription();
        link = beaconModel.getLink();
        groupToBind = beaconModel.getGroupToBind();
        groupName = beaconModel.getGroupName();
        absoluteMapFolderUrl = beaconModel.getAbsoluteMapUrl();
        mapWidth = beaconModel.getMapWidth();
        mapHeight = beaconModel.getMapHeight();
        beaconPinX = beaconModel.getBeaconPinX();
        beaconPinY = beaconModel.getBeaconPinY();

        additionalContent = beaconModel.getContent();
        favorite = beaconModel.getFavorite();

    }

    public int getNextShownContent() {
        return nextShownContent;
    }

    public void setNextShownContent(int lastShownContent) {
        this.nextShownContent = lastShownContent;
    }

    public List<BeaconAdditionalContent> getAdditionalContent() {
        return additionalContent;
    }

    public void setAdditionalContent(List<BeaconAdditionalContent> additionalContent) {
        this.additionalContent = additionalContent;
    }

    @Override
    public String toString() {
        return "DBBeacon{" +
                "ids=" + ids +
                ", title='" + title + '\'' +
                ", picture='" + picture + '\'' +
                ", link='" + link + '\'' +
                ", description='" + description + '\'' +
                ", nextShowTime=" + nextShowTime +
                ", nextupdateTime=" + nextUpdateTime +
                ", groupToBind=" + groupToBind +
                ", absoluteMapFolderUrl=" + absoluteMapFolderUrl +
                ", mapWidth=" + mapWidth +
                ", mapHeight=" + mapHeight +
                ", beaconPinX=" + beaconPinX +
                ", beaconPinY=" + beaconPinY +
                '}';
    }

    public BeaconIds getIds() {
        return ids;
    }

    public void setIds(BeaconIds ids) {
        this.ids = ids;
    }

    public long getNextShowTime() {
        return nextShowTime;
    }

    public void setNextShowTime(long nextShowTime) {
        this.nextShowTime = nextShowTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String image) {
        this.picture = image;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getNextUpdateTime() {
        return nextUpdateTime;
    }

    public void setNextUpdateTime(long nextUpdateTime) {
        this.nextUpdateTime = nextUpdateTime;
    }

    public String getGroupToBind() {
        return groupToBind;
    }

    public void setGroupToBind(String groupToBind) {
        this.groupToBind = groupToBind;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getAbsoluteMapFolderUrl() {
        return absoluteMapFolderUrl;
    }

    public void setAbsoluteMapFolderUrl(String absoluteMapFolderUrl) {
        this.absoluteMapFolderUrl = absoluteMapFolderUrl;
    }

    public int getMapWidth() {
        return mapWidth;
    }

    public void setMapWidth(int mapWidth) {
        this.mapWidth = mapWidth;
    }

    public int getMapHeight() {
        return mapHeight;
    }

    public void setMapHeight(int mapHeight) {
        this.mapHeight = mapHeight;
    }

    public double getBeaconPinX() {
        return beaconPinX;
    }

    public void setBeaconPinX(double beaconPinX) {
        this.beaconPinX = beaconPinX;
    }

    public double getBeaconPinY() {
        return beaconPinY;
    }

    public void setBeaconPinY(double beaconPinY) {
        this.beaconPinY = beaconPinY;
    }

    public int getFavorite() { return favorite; }

    public void setFavorite(int favorite) { this.favorite = favorite; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DBBeacon dbBeacon = (DBBeacon) o;

        if (mapWidth != dbBeacon.mapWidth) return false;
        if (mapHeight != dbBeacon.mapHeight) return false;
        if (Double.compare(dbBeacon.beaconPinX, beaconPinX) != 0) return false;
        if (Double.compare(dbBeacon.beaconPinY, beaconPinY) != 0) return false;
        if (!ids.equals(dbBeacon.ids)) return false;
        if (!title.equals(dbBeacon.title)) return false;
        if (!picture.equals(dbBeacon.picture)) return false;
        if (!description.equals(dbBeacon.description)) return false;
        if (!link.equals(dbBeacon.link)) return false;
        if (!groupToBind.equals(dbBeacon.groupToBind)) return false;
        if (!groupName.equals(dbBeacon.groupName)) return false;
        return absoluteMapFolderUrl.equals(dbBeacon.absoluteMapFolderUrl);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = ids.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + picture.hashCode();
        result = 31 * result + description.hashCode();
        result = 31 * result + link.hashCode();
        result = 31 * result + groupToBind.hashCode();
        result = 31 * result + groupName.hashCode();
        result = 31 * result + absoluteMapFolderUrl.hashCode();
        result = 31 * result + mapWidth;
        result = 31 * result + mapHeight;
        temp = Double.doubleToLongBits(beaconPinX);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(beaconPinY);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}