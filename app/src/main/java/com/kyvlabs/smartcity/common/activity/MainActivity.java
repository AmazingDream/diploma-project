package com.kyvlabs.smartcity.common.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.api.GoogleApiClient;
import com.kyvlabs.smartcity.Application;


import com.kyvlabs.smartcity.common.adapter.CSRicyclerAdapter;
import com.kyvlabs.smartcity.data.BeaconIds;
import com.kyvlabs.smartcity.data.Coordinates;
import com.kyvlabs.smartcity.data.DBBeacon;
import com.kyvlabs.smartcity.data.DBHelper;
import com.kyvlabs.smartcity.dialogs.DialogHelper;
import com.kyvlabs.smartcity.services.LocateService;
import com.kyvlabs.smartcity.smartcity.R;
import com.pkmmte.view.CircularImageView;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;

@SuppressWarnings("WeakerAccess")
public class MainActivity extends AppCompatActivity implements BeaconConsumer {

    Activity activity;

    protected static final String LOG_TAG = "NearbyActivity";
    private BeaconManager beaconManager;
    private static final int BT_REQUEST = 102;

    private static final int LAYOUT = R.layout.activity_main;


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.navigation)
    NavigationView navigationView;




    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppDefault);
        super.onCreate(savedInstanceState);
        setContentView(LAYOUT);
        ButterKnife.bind(this);
        activity = this;
        initToolbar();
        initNavigationView();

        DBHelper dbHelper = new DBHelper();
        dbHelper.setCoordinates(new BeaconIds("ebefd083-70a2-47c8-9837-e7b5634df524","1","1"), 47.8176353, 35.1840204);
        dbHelper.setCoordinates(new BeaconIds("ebefd083-70a2-47c8-9837-e7b5634df524","1","5"), 47.81861379, 35.18039659);
        dbHelper.setCoordinates(new BeaconIds("ebefd083-70a2-47c8-9837-e7b5634df524","1","4"), 47.81793839, 35.18575966);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN) != PackageManager.PERMISSION_GRANTED) {

            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding


            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN},
                    BT_REQUEST);
        }
        if (savedInstanceState == null) {
            MainActivity.PlaceholderFragment listFragment = new MainActivity.PlaceholderFragment();
            getFragmentManager().beginTransaction()
                    .add(R.id.container, listFragment)
                    .commit();
        }
        beaconManager = BeaconManager.getInstanceForApplication(this);
        List<BeaconParser> beaconParsers = beaconManager.getBeaconParsers();
        beaconParsers.add(new BeaconParser().
                setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        //beaconManager parametrizations
        beaconManager.setBackgroundScanPeriod(Long.valueOf(sharedPreferences.getString(getStrRes(R.string.settings_key_background_scan_period), getStrRes(R.string.settings_default_background_scan_period))));
        beaconManager.setBackgroundBetweenScanPeriod(Long.valueOf(sharedPreferences.getString(getStrRes(R.string.settings_key_background_between_scan_period), getStrRes(R.string.settings_default_background_between_scan_period))));
        beaconManager.setForegroundScanPeriod(Long.valueOf(sharedPreferences.getString(getStrRes(R.string.settings_key_foreground_scan_period), getStrRes(R.string.settings_default_foreground_scan_period))));
        beaconManager.setForegroundBetweenScanPeriod(Long.valueOf(sharedPreferences.getString(getStrRes(R.string.settings_key_foreground_between_scan_period), getStrRes(R.string.settings_default_foreground_between_scan_period))));

    }


    private static void getFavorites() {
        DBHelper dbHelper = new DBHelper();
        MainActivity.PlaceholderFragment.setBeacons(new ArrayList<>(dbHelper.getFavorites()));
        dbHelper.close();
    }
    private void initToolbar() {
        toolbar.setTitle(R.string.app_name);
    }


    private void initNavigationView() {
       ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.key_action_bar_vew_navigation_open, R.string.key_action_bar_vew_navigation_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        setCheckedNavigationDrawerMenuItem(R.id.navigation_action_beacons);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                drawerLayout.closeDrawers();
                switch (item.getItemId()) {
                    case R.id.navigation_action_beacons: {
                        Intent intent = new Intent(activity, MainActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                    }
                    case R.id.navigation_action_nearby: {
                        activity.stopService(new Intent(activity, LocateService.class));
                        Intent intent = new Intent(activity, NearbyActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                    }
                    case R.id.navigation_action_locate: {
                        activity.startService(new Intent(activity, LocateService.class));
                        Toast.makeText(activity, R.string.scan_started, Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                    }
                    case R.id.navigation_action_stopLocating: {
                        activity.stopService(new Intent(activity, LocateService.class));
                        Toast.makeText(activity, R.string.scan_stopped, Toast.LENGTH_SHORT).show();
                        break;
                    }
                    case R.id.navigation_action_favorite: {
                        Intent intent = new Intent(activity, FavoriteActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                    }
                    case R.id.navigation_action_logout: {
                        SharedPreferences preferences = getSharedPreferences(getString(R.string.saved_auth_key), 0);
                        preferences.edit().remove(getString(R.string.saved_auth_key)).commit();
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                        finish();
                        break;
                    }
                    case R.id.navigation_action_settings: {
                        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_action_about: {
                        Intent intent2 = new Intent(MainActivity.this, AboutActivity.class);
                        startActivity(intent2);
                        break;
                    }
                }
                return true;
            }
        });

        View headerView = LayoutInflater.from(this).inflate(R.layout.navigation_drawer_header, navigationView, false);
        navigationView.addHeaderView(headerView);

        CircularImageView faceImageView = ButterKnife.findById(headerView, R.id.brrr_logo);

        try {
            InputStream ims = getAssets().open("brrr.jpg");
            Drawable d = Drawable.createFromStream(ims, null);
            faceImageView.setImageDrawable(d);
        } catch (IOException e) {
            e.printStackTrace();
        }

   /*     TextView nameTextView = ButterKnife.findById(headerView, R.id.name);
        nameTextView.setText(R.string.default_drawer_text);

        TextView emailTextView = ButterKnife.findById(headerView, R.id.email);
        emailTextView.setText(R.string.default_drawer_mail);*/
    }

    private void setCheckedNavigationDrawerMenuItem(int navigationId) {
        navigationView.setCheckedItem(navigationId);
    }


    private String getStrRes(int intResource) {
        return this.getResources().getString(intResource);
    }

    @Override
    protected void onResume() {
        super.onResume();
        beaconManager.bind(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        beaconManager.unbind(this);
    }

    @Override
    public void onBeaconServiceConnect() {
        beaconManager.setRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
///////////////
                ArrayList<Identifier> identifierArrayList = new ArrayList<>();

                identifierArrayList.add(Identifier.parse("3909f602-de9c-44e7-b228-2b91d8ff1f77"));
                identifierArrayList.add(Identifier.parse("1"));
                identifierArrayList.add(Identifier.parse("1"));

                Beacon.Builder builder = new Beacon.Builder().setIdentifiers(identifierArrayList);
                Beacon build = builder.build();

                beacons.add(build);
///////////////

                Log.d(LOG_TAG, "FOUNDED " + beacons.size() + " beacons");

                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                Integer beacon_range = Integer.valueOf(sharedPreferences.getString(getStrRes(R.string.settings_key_beacon_range), getStrRes(R.string.settings_default_beacon_range)));
                Iterator<Beacon> iterator = beacons.iterator();
                while (iterator.hasNext()) {
                    Beacon next = iterator.next();
                    Log.d(LOG_TAG, "Distance " + next.getDistance() + " beacons");
                    if (next.getDistance() > beacon_range) {
                        iterator.remove();
                    }
                }

                Log.d(LOG_TAG, "Filtered by distance" + beacon_range + ": " + beacons.size() + " beacons");

                getFavorites();
                MainActivity.PlaceholderFragment.setBeacons(new ArrayList<>(getIdsFromBeacons(beacons)));
            }
        });
        try {
            beaconManager.startRangingBeaconsInRegion(new Region(getStrRes(R.string.region_name), null, null, null));
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private Collection<BeaconIds> getIdsFromBeacons(Collection<Beacon> beacons) {
        Collection<BeaconIds> beaconIdsList = new ArrayList<>();
        for (Beacon beacon : beacons) {
            beaconIdsList.add(new BeaconIds(beacon.getId1().toString(), beacon.getId2().toString(), beacon.getId3().toString()));
        }
        return beaconIdsList;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {

        switch (requestCode) {
            case BT_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Bluetooth permission granted", Toast.LENGTH_LONG);
                }
            }
        }
    }

    public static class PlaceholderFragment extends Fragment {
        private static List<DBBeacon> beaconsFromDB = new ArrayList<>();
        // private static AdsListViewAdapter beaconsAdapter;
        private static CSRicyclerAdapter beaconsAdapter;
        private static ListView adsListView;
        private static View rootView;
        private static final int LAYOUT = R.layout.fragment_city_stream;
        @BindView(R.id.city_stream_recycler_view)
        RecyclerView mRecyclerView;
        @BindView(R.id.city_stream_swipe_refresh_layout)
        SwipeRefreshLayout mSwipeRefreshLayout;

        private static Handler UIHandler = new Handler(Looper.getMainLooper());

        public PlaceholderFragment() {
        }

        //set beacons collection to list after reading from db
        public static void setBeacons(Collection<BeaconIds> beacons) {
            getBeaconsFromDB(beacons);
        }

        //After scanning reading founded beacons list from dB.
        private static void getBeaconsFromDB(Collection<BeaconIds> beaconCollection) {

            SharedPreferences userDetails = Application.getAppContext().getSharedPreferences(Application.getAppContext().getString(R.string.saved_auth_key), Context.MODE_PRIVATE);
            String auth = userDetails.getString(Application.getAppContext().getString(R.string.saved_auth_key), "");

            DBHelper dbHelper = new DBHelper();
            dbHelper.getBeaconsByIds(beaconCollection, auth)
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(new Action0() {
                        @Override
                        public void call() {
                            clearAdapter();
                        }
                    })
                    .subscribe(
                            new Action1<DBBeacon>() {
                                @Override
                                public void call(final DBBeacon beacon) {
                                    addBeaconToAdapter(beacon);
                                }
                            },
                            new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {
                                    Log.d("Talk on error", "", throwable);
                                }
                            });

            dbHelper.close();

        }

        private static void clearAdapter() {
            runOnUi(new Runnable() {
                @Override
                public void run() {
                    beaconsFromDB.clear();
                    beaconsAdapter.notifyDataSetChanged();
                }
            });
        }

        private static void addBeaconToAdapter(final DBBeacon beacon) {
            runOnUi(new Runnable() {
                @Override
                public void run() {
                    Set<DBBeacon> set = new HashSet<>(beaconsFromDB);
                    if (!set.contains(beacon)) {
                        beaconsFromDB.add(beacon);
                        beaconsAdapter.notifyDataSetChanged();
                    }
                }
            });
        }

        private static void runOnUi(Runnable runnable) {
            UIHandler.post(runnable);
        }

        @Override
        public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
            switch (requestCode) {
                case 0: {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.length <= 0
                            || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(getActivity(), "Sorry but we need this permission", Toast.LENGTH_LONG).show();
                    }
                }

            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    Toast.makeText(getActivity(), "Sorry but we need this permission", Toast.LENGTH_LONG).show();
                    getActivity().finish();
                } else {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                            0);
                }
            }

            rootView = inflater.inflate(LAYOUT, container, false);
            ButterKnife.bind(this, rootView);
            beaconsAdapter = new CSRicyclerAdapter(beaconsFromDB);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mRecyclerView.setAdapter(beaconsAdapter);
            mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            verifyBluetooth();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    },2000);
                }
            });

            return rootView;
        }
        private void verifyBluetooth() {
            try {
                if (!BeaconManager.getInstanceForApplication(getActivity()).checkAvailability()) {
                    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    if (!mBluetoothAdapter.isEnabled()) {
                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBtIntent, BT_REQUEST);
                    }

                }
            } catch (RuntimeException e) {
                showBTLENotSupportedMessage();
            }
        }
        private void showBTLENotSupportedMessage() {
            DialogHelper.showOkDialog(getActivity(), R.string.bluetooth_activation_request_title, R.string.bluetooth_activation_request_message);
        }
        @Override
        public void onStart() {
            super.onStart();
            clearAdapter();
        }

        @Override
        public void onResume() {
            super.onResume();
            clearAdapter();
        }
    }


}