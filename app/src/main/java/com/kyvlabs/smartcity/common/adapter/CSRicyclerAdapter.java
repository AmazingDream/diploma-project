package com.kyvlabs.smartcity.common.adapter;

/**
 * Created by Никита on 30.03.2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kyvlabs.smartcity.Application;
import com.kyvlabs.smartcity.common.activity.AdvertActivity;
import com.kyvlabs.smartcity.common.network.model.BeaconAdditionalContent;
import com.kyvlabs.smartcity.data.DBBeacon;
import com.kyvlabs.smartcity.data.DBHelper;
import com.kyvlabs.smartcity.data.DataKeys;
import com.kyvlabs.smartcity.smartcity.BuildConfig;
import com.kyvlabs.smartcity.smartcity.MapsActivity;
import com.kyvlabs.smartcity.smartcity.R;
import com.kyvlabs.smartcity.views.HtmlTagHandler;
import com.squareup.picasso.Picasso;

import java.util.List;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class CSRicyclerAdapter extends RecyclerView.Adapter<CSRicyclerAdapter.ViewHolder> {
    private final List<DBBeacon> mItems;
    DBHelper dbHelper;
    public CSRicyclerAdapter(List<DBBeacon> beacons) {
        this.mItems = beacons;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.stream_card, viewGroup, false);
        return new ViewHolder(v);
    }
    @Override
    public void onBindViewHolder(CSRicyclerAdapter.ViewHolder viewHolder, int i) {
        viewHolder.setData(mItems.get(i));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

   /* public void addItem(DBBeacon beacon) {
        this.mItems.addFirst(beacon);
    }
*/

    public Object getItem(int position) {
       return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.card_title_image)
        ImageView titleImage;
        @BindView(R.id.card_title_text)
        TextView titleText;
        @BindView(R.id.card_time_text)
        TextView timeText;
        @BindView(R.id.card_main_text)
        TextView mainText;
        @BindView(R.id.card_action_button1)
        Button actionButton1;
        @BindView(R.id.card_action_button2)
        Button actionButton2;
        @BindView(R.id.card_like_button)
        ImageButton cardHeartButton;

        private boolean mainTextExpand = false;
        private DBBeacon beacon;


        ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }

        public void setData(DBBeacon beacon) {
            this.beacon = beacon;

            Picasso picasso = Picasso.with(Application.getAppContext());
            if (BuildConfig.DEBUG) {
                picasso.setIndicatorsEnabled(true);
            }
            picasso.load(beacon.getPicture())
                    .error(R.drawable.no_image)
                    .into(titleImage);

         //   titleText.setText(beacon.getTitle());
            timeText.setText(beacon.getTitle());
            mainText.setText(Html.fromHtml(beacon.getDescription(), null, new HtmlTagHandler()));
            actionButton1.setText(R.string.card_button1_text);
            actionButton2.setText(R.string.card_button2_text);
            dbHelper = new DBHelper();
            if(dbHelper.isFavorite(beacon.getIds()))
                cardHeartButton.setSelected(true);
            else
                cardHeartButton.setSelected(false);
            dbHelper.close();
        }

        @OnClick(R.id.card_like_button)
        public void likeToggle(View button) {
            dbHelper = new DBHelper();
            button.setSelected(!button.isSelected());
            if (button.isSelected()) {
                like();
            } else {
                disLike();
            }
            dbHelper.close();
        }

        @OnClick(R.id.card_action_button1)
        public void showOnMap() {
            Intent intent = new Intent(actionButton1.getContext(), MapsActivity.class);
            intent.putExtra("title", beacon.getTitle());
            intent.putExtra("description", beacon.getDescription());
            intent.putExtra("x", beacon.getBeaconPinX());
            intent.putExtra("y", beacon.getBeaconPinY());
            intent.putExtra("ids", beacon.getIds());
            actionButton1.getContext().startActivity(intent);
         /*   String str;
            str = "geo:" + beacon.getBeaconPinX() + "," + beacon.getBeaconPinY();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(str));
            actionButton1.getContext().startActivity(intent);
*/
        }

        @OnClick(R.id.card_action_button2)
        public void getInfo() {
            Intent intent = new Intent(actionButton2.getContext(), AdvertActivity.class);
            int nextShownContent = beacon.getNextShownContent();

            if (nextShownContent <= 0) {
                intent.putExtra(DataKeys.AD_TITLE_KEY, beacon.getTitle());
                intent.putExtra(DataKeys.AD_PICTURE_KEY, beacon.getPicture());
                intent.putExtra(DataKeys.AD_LINK_KEY, beacon.getLink());
                intent.putExtra(DataKeys.AD_DESCRIPTION_KEY, beacon.getDescription());
            } else {
                BeaconAdditionalContent beaconAdditionalContent = beacon.getAdditionalContent().get(nextShownContent - 1);
                intent.putExtra(DataKeys.AD_TITLE_KEY, beaconAdditionalContent.getTitle());
                intent.putExtra(DataKeys.AD_PICTURE_KEY, beaconAdditionalContent.getAbsolutePicture());
                intent.putExtra(DataKeys.AD_LINK_KEY, beaconAdditionalContent.getLink());
                intent.putExtra(DataKeys.AD_DESCRIPTION_KEY, beaconAdditionalContent.getDescription());
            }

            intent.putExtra(DataKeys.BEACON_IDS, beacon.getIds());
            intent.putExtra(DataKeys.AD_GROUP_NAME, beacon.getGroupName());
            intent.putExtra(DataKeys.AD_TABLE_MAP_WIDTH, beacon.getMapWidth());
            intent.putExtra(DataKeys.AD_TABLE_MAP_HEIGTH, beacon.getMapHeight());
            intent.putExtra(DataKeys.AD_TABLE_BEACON_PIN_X, beacon.getBeaconPinX());
            intent.putExtra(DataKeys.AD_TABLE_BEACON_PIN_Y, beacon.getBeaconPinY());
            intent.putExtra(DataKeys.AD_TABLE_ABSOLUTE_MAP_FOLDER_URL, beacon.getAbsoluteMapFolderUrl());

            new DBHelper().updateNextShownContent(beacon);

            actionButton2.getContext().startActivity(intent);
        }

        private void like() {
            dbHelper.addToFavorites(beacon.getIds());
        }

        private void disLike() {
            dbHelper.removeFromFavorites(beacon.getIds());
        }

        @OnClick(R.id.card_main_text)
        public void expandMainTextClick(View v) {
            mainTextExpand = !mainTextExpand;
            if (mainTextExpand) {
                ((TextView) v).setSingleLine(false);
            } else {
                ((TextView) v).setLines(2);
            }
        }

    }
}
