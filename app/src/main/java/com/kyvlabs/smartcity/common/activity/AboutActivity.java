package com.kyvlabs.smartcity.common.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kyvlabs.smartcity.smartcity.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}
