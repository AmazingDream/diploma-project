package com.kyvlabs.smartcity.common.network.model;

public class ForgotModel {
    private boolean status;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
