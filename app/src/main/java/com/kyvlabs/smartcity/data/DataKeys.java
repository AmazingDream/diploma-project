package com.kyvlabs.smartcity.data;

/**
 * Keys for moving data between activities
 */
public class DataKeys {

    //Beacon Ids
    public static final String BEACON_IDS = "ids";
    public static final String BEACON_IDS_LIST = "ids_list";
    //Advert beacon information
    public static final String AD_PICTURE_KEY = "ad_picture";
    public static final String AD_TITLE_KEY = "ad_title";
    public static final String AD_DESCRIPTION_KEY = "ad_description";
    public static final String AD_GROUP_NAME = "group_name";
    public static final String AD_TABLE_MAP_WIDTH = "map_width";
    public static final String AD_TABLE_MAP_HEIGTH = "map_height";
    public static final String AD_TABLE_BEACON_PIN_X = "beacon_pin_x";
    public static final String AD_TABLE_BEACON_PIN_Y = "beacon_pin_Y";
    public static final String AD_TABLE_ABSOLUTE_MAP_FOLDER_URL = "absolute_map_folder_url";

    //Advert fragment reason
    public static final String ADVERT_REASON_KEY = "advert_reason";
    public static final String ADVERT_WALK_REASON = "advert_walk_reason";

    public static final String AD_LINK_KEY = "advert_link";
}
