package com.kyvlabs.smartcity.common.network.model;

public class BeaconAdditionalContent {
    private String id;
    private String beacon_id;
    private String title;
    private String link;
    private String description;
    private String picture;
    private String horizontal_picture;
    private String additional_info;
    private String absolutePicture;
    private String absoluteHorizontalPicture;

    @Override
    public String toString() {
        return "BeaconAdditionalContent{" +
                "id=" + id +
                ", beacon_id=" + beacon_id +
                ", title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", description='" + description + '\'' +
                ", picture='" + picture + '\'' +
                ", horizontal_picture='" + horizontal_picture + '\'' +
                ", additional_info='" + additional_info + '\'' +
                ", absolutePicture='" + absolutePicture + '\'' +
                ", absoluteHorizontalPicture='" + absoluteHorizontalPicture + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBeacon_id() {
        return beacon_id;
    }

    public void setBeacon_id(String beacon_id) {
        this.beacon_id = beacon_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getHorizontal_picture() {
        return horizontal_picture;
    }

    public void setHorizontal_picture(String horizontal_picture) {
        this.horizontal_picture = horizontal_picture;
    }

    public String getAdditional_info() {
        return additional_info;
    }

    public void setAdditional_info(String additional_info) {
        this.additional_info = additional_info;
    }

    public String getAbsolutePicture() {
        return absolutePicture;
    }

    public void setAbsolutePicture(String absolutePicture) {
        this.absolutePicture = absolutePicture;
    }

    public String getAbsoluteHorizontalPicture() {
        return absoluteHorizontalPicture;
    }

    public void setAbsoluteHorizontalPicture(String absoluteHorizontalPicture) {
        this.absoluteHorizontalPicture = absoluteHorizontalPicture;
    }
}
