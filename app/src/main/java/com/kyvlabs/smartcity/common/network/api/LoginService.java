package com.kyvlabs.smartcity.common.network.api;

import com.kyvlabs.smartcity.common.network.model.ForgotModel;
import com.kyvlabs.smartcity.common.network.model.Group;
import com.kyvlabs.smartcity.common.network.model.LoginModel;
import com.kyvlabs.smartcity.common.network.model.RegistrationModel;

import java.util.ArrayList;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import rx.Observable;

public interface LoginService {

    @POST("/api/register/")
    @FormUrlEncoded
    Observable<RegistrationModel> register(@Field(value = "ClientUsers[email]") String email, @Field(value = "ClientUsers[password]") String pass, @Field(value = "ClientUsers[group_ids]") String jsonGroupsArray);

    @POST("/api/login/")
    @FormUrlEncoded
    Observable<LoginModel> login(@Field(value = "ClientUsers[email]") String email, @Field(value = "ClientUsers[password]") String pass, @Field(value = "ClientUsers[group_ids]") String jsonGroupsArray);

    @POST("/api/fb-auth/")
    @FormUrlEncoded
    Observable<LoginModel> loginFb(@Field(value = "ClientUsers[email]") String email, @Field(value = "ClientUsers[password]") String pass, @Field(value = "ClientUsers[auth_key]") String fbAuth, @Field(value = "ClientUsers[group_ids]") String jsonGroupsArray);

    @POST("/api/password-restore/")
    @FormUrlEncoded
    Observable<ForgotModel> forgotPassword(@Field(value = "ClientUsers[email]") String email);

    @GET("smart-city/categories")
    Observable<ArrayList<Group>> getGroups();
}
