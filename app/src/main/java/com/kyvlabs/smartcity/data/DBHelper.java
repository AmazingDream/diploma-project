package com.kyvlabs.smartcity.data;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.util.Log;

import com.kyvlabs.smartcity.Application;
import com.kyvlabs.smartcity.smartcity.R;
import com.kyvlabs.smartcity.common.network.NetworkHelper;
import com.kyvlabs.smartcity.common.network.model.BeaconAdditionalContent;
import com.kyvlabs.smartcity.common.network.model.BeaconModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import rx.Observable;
import rx.functions.Func1;

//Helper to work with DB.
public class DBHelper extends SQLiteOpenHelper {

    private static final int DB_VERSION = 71;

    private static final String DB_NAME = "adsDB";

    private static final String AD_TABLE = "ads";

    private static final String AD_TABLE_UUID = "uuid";
    private static final String AD_TABLE_MAJOR = "major";
    private static final String AD_TABLE_MINOR = "minor";
    private static final String AD_TABLE_TITLE = "title";
    private static final String AD_TABLE_PICTURE = "picture";
    private static final String AD_TABLE_LINK = "link";
    private static final String AD_TABLE_DESCRIPTION = "description";
    private static final String AD_TABLE_NEXT_SHOW_TIME = "last_next_time";
    private static final String AD_TABLE_NEXT_UPDATE_TIME = "last_update_time";
    private static final String AD_TABLE_GROUP_ID = "group_id";
    private static final String AD_TABLE_GROUP_NAME = "group_name";
    private static final String AD_TABLE_MAP_WIDTH = "map_width";
    private static final String AD_TABLE_MAP_HEIGTH = "map_height";
    private static final String AD_TABLE_BEACON_PIN_X = "beacon_pin_x";
    private static final String AD_TABLE_BEACON_PIN_Y = "beacon_pin_Y";
    private static final String AD_TABLE_ABSOLUTE_MAP_FOLDER_URL = "absolute_map_folder_url";
    private static final String AD_TABLE_NEXT_CONTENT = "lastshown_content";
    private static final String AD_TABLE_FAVORITE = "favorite";

    private static final String ADDITIONAL_TABLE = "additional";

    private static final String ADDITIONAL_TABLE_BEACON_UUID = "additional_uuid";
    private static final String ADDITIONAL_TABLE_ID = "additional_id";
    private static final String ADDITIONAL_TABLE_BEACON_ID = "additional_beacon_id";
    private static final String ADDITIONAL_TABLE_TITLE = "additional_title";
    private static final String ADDITIONAL_TABLE_LINK = "additional_link";
    private static final String ADDITIONAL_TABLE_DESCRIPTION = "additional_description";
    private static final String ADDITIONAL_TABLE_PICTURE = "additional_picture";
    private static final String ADDITIONAL_TABLE_ABSOLUTE_PPICTURE = "additional_absolutePicture";

    private static final String BLACK_LIST_TABLE = "black_list";

    private static final String BLACK_LIST_TABLE_UUID = "uuid";
    private static final String BLACK_LIST_TABLE_MAJOR = "major";
    private static final String BLACK_LIST_TABLE_MINOR = "minor";
    private static final String BLACK_LIST_TABLE_TITLE = "title";
    private static final String BLACK_LIST_TABLE_DESCRIPTION = "description";


    private static final String FAVORITES_TABLE = "favorites";

    private static final String FAVORITES_TABLE_UUID = "uuid";
    private static final String FAVORITES_TABLE_MAJOR = "major";
    private static final String FAVORITES_TABLE_MINOR = "minor";
    private static final String FAVORITES_TABLE_TITLE = "title";
    private static final String FAVORITES_TABLE_PICTURE = "picture";
    private static final String FAVORITES_TABLE_LINK = "link";
    private static final String FAVORITES_TABLE_DESCRIPTION = "description";
    private static final String FAVORITES_TABLE_GROUP_ID = "group_id";
    private static final String FAVORITES_TABLE_GROUP_NAME = "group_name";
    private static final String FAVORITES_TABLE_BEACON_PIN_X = "beacon_pin_x";
    private static final String FAVORITES_TABLE_BEACON_PIN_Y = "beacon_pin_Y";

    private static final String COORDINATES_TABLE = "coordinates";
    private static final String COORDINATES_TABLE_UUID = "uuid";
    private static final String COORDINATES_TABLE_MAJOR = "major";
    private static final String COORDINATES_TABLE_MINOR = "minor";
    private static final String COORDINATES_TABLE_X = "X";
    private static final String COORDINATES_TABLE_Y = "Y";

    private static long NOT_UPDATE_INTERVAL = 1000 * 10;//Default interval 10sec.


    //Application context
    private Context context;

    public DBHelper() {
        super(Application.getAppContext(), DB_NAME, null, DB_VERSION);
        this.context = Application.getAppContext();
    }

    //Read All beacons from db
    public List<DBBeacon> getBeaconsFromDB() {
        ArrayList<DBBeacon> beaconsFromDB = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + AD_TABLE, null);
        while (cursor.moveToNext()) {
            DBBeacon beacon = getBeaconFromCursor(cursor);
            beaconsFromDB.add(beacon);
        }
        db.close();
        return beaconsFromDB;
    }

    // get beacon from cursor
    private DBBeacon getBeaconFromCursor(Cursor cursor) {
        DBBeacon beacon = new DBBeacon();
        BeaconIds ids = new BeaconIds(cursor.getString(cursor.getColumnIndex(AD_TABLE_UUID)),
                cursor.getString(cursor.getColumnIndex(AD_TABLE_MAJOR)),
                cursor.getString(cursor.getColumnIndex(AD_TABLE_MINOR)));
        beacon.setIds(ids);
        beacon.setTitle(cursor.getString(cursor.getColumnIndex(AD_TABLE_TITLE)));
        beacon.setPicture(cursor.getString(cursor.getColumnIndex(AD_TABLE_PICTURE)));
        beacon.setLink(cursor.getString(cursor.getColumnIndex(AD_TABLE_LINK)));
        beacon.setDescription(cursor.getString(cursor.getColumnIndex(AD_TABLE_DESCRIPTION)));
        beacon.setNextShowTime(cursor.getLong(cursor.getColumnIndex(AD_TABLE_NEXT_SHOW_TIME)));
        beacon.setNextUpdateTime(cursor.getLong(cursor.getColumnIndex(AD_TABLE_NEXT_UPDATE_TIME)));
        beacon.setGroupName(cursor.getString(cursor.getColumnIndex(AD_TABLE_GROUP_NAME)));
        beacon.setGroupToBind(cursor.getString(cursor.getColumnIndex(AD_TABLE_GROUP_ID)));
        beacon.setMapWidth(cursor.getInt(cursor.getColumnIndex(AD_TABLE_MAP_WIDTH)));
        beacon.setMapHeight(cursor.getInt(cursor.getColumnIndex(AD_TABLE_MAP_HEIGTH)));
        beacon.setBeaconPinX(cursor.getDouble(cursor.getColumnIndex(AD_TABLE_BEACON_PIN_X)));
        beacon.setBeaconPinY(cursor.getDouble(cursor.getColumnIndex(AD_TABLE_BEACON_PIN_Y)));
        beacon.setAbsoluteMapFolderUrl(cursor.getString(cursor.getColumnIndex(AD_TABLE_ABSOLUTE_MAP_FOLDER_URL)));
        beacon.setNextShownContent(cursor.getInt(cursor.getColumnIndex(AD_TABLE_NEXT_CONTENT)));
        beacon.setFavorite(cursor.getInt(cursor.getColumnIndex(AD_TABLE_FAVORITE)));
        return beacon;
    }

    //set next show time
    public void setNextShowTime(BeaconIds ids, long timestamp) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(AD_TABLE_NEXT_SHOW_TIME, timestamp);
        String whereClause = AD_TABLE_UUID + " =? AND " + AD_TABLE_MAJOR + " =? AND " + AD_TABLE_MINOR + "=? ";
        String[] whereArgs = new String[]{ids.getUuid(), ids.getMajor(), ids.getMinor()};
        db.update(AD_TABLE, cv, whereClause, whereArgs);
    }

    // set all beacons show time to 0. All beacons shows next time.
    public void clearAllNextShowTimes() {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(AD_TABLE_NEXT_SHOW_TIME, 0);
        String whereClause = AD_TABLE_NEXT_SHOW_TIME + " > ?";
        String[] whereArgs = new String[]{"0"};
        db.update(AD_TABLE, cv, whereClause, whereArgs);
    }

    public void removeAllBeaconsData() {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(AD_TABLE, null, null);
        db.delete(ADDITIONAL_TABLE, null, null);
    }

    //Returns beacon list by ids collection
    public Observable<DBBeacon> getBeaconsByIds(Collection<BeaconIds> beaconCollection, final String auth) {
        final NetworkHelper networkHelper = new NetworkHelper();
        Observable<DBBeacon> beaconsDBCollection = Observable.from(beaconCollection)
                .map(new Func1<BeaconIds, DBBeacon>() {
                    @Override
                    public DBBeacon call(BeaconIds beaconIds) {
                        return changeToDbBeacon(beaconIds);
                    }
                });

        Observable<DBBeacon> beaconsToShow;
        if (networkHelper.isOnline()) {
            beaconsToShow = beaconsDBCollection
                    .map(new Func1<DBBeacon, BeaconIds>() {
                        @Override
                        public BeaconIds call(DBBeacon beacon) {
                            return beacon.getIds();
                        }
                    })
                    .filter(new Func1<BeaconIds, Boolean>() {
                        @Override
                        public Boolean call(BeaconIds ids) {
                            return !isOnBlackList(ids);
                        }
                    })
                    .toList()
                    .flatMap(new Func1<List<BeaconIds>, Observable<BeaconModel>>() {
                        @Override
                        public Observable<BeaconModel> call(List<BeaconIds> beaconIdses) {
                            if (beaconIdses.size() > 0) {
                                return networkHelper.getBeacons(beaconIdses, auth);
                            } else {
                                return Observable.empty();
                            }
                        }
                    })
                    .map(new Func1<BeaconModel, DBBeacon>() {
                        @Override
                        public DBBeacon call(BeaconModel beaconModel) {
                            return new DBBeacon(beaconModel);
                        }
                    })
                    .map(new Func1<DBBeacon, DBBeacon>() {
                        @Override
                        public DBBeacon call(DBBeacon dbBeacon) {
                            dbBeacon.setNextShownContent(getNextShownContentFromDB(dbBeacon.getIds()));
                            return dbBeacon;
                        }
                    })
                    .map(new Func1<DBBeacon, DBBeacon>() {
                        @Override
                        public DBBeacon call(DBBeacon dbBeacon) {
                            writeAdditionsToDb(dbBeacon);
                            return dbBeacon;
                        }
                    })
                    .map(new Func1<DBBeacon, DBBeacon>() {
                        @Override
                        public DBBeacon call(DBBeacon beacon) {
                            addOrUpdateBeacon(beacon);
                            return beacon;
                        }
                    })
            ;
        } else {
            beaconsToShow = Observable.from(beaconCollection)
                    .filter(new Func1<BeaconIds, Boolean>() {
                        @Override
                        public Boolean call(BeaconIds ids) {
                            return !isOnBlackList(ids);
                        }
                    })
                    .map(new Func1<BeaconIds, DBBeacon>() {
                        @Override
                        public DBBeacon call(BeaconIds beaconIds) {
                            return getBeaconByIds(beaconIds);
                        }
                    })
                    .filter(new Func1<DBBeacon, Boolean>() {
                        @Override
                        public Boolean call(DBBeacon beacon) {
                            return (beacon.getNextUpdateTime() > System.currentTimeMillis());
                        }
                    })
                    .map(new Func1<DBBeacon, DBBeacon>() {
                        @Override
                        public DBBeacon call(DBBeacon dbBeacon) {
                            dbBeacon.setAdditionalContent(readAdditionalContent(dbBeacon.getIds().toString()));
                            return dbBeacon;
                        }
                    })
            ;
        }

        return beaconsToShow;
    }

    private int getNextShownContentFromDB(BeaconIds ids) {
        DBBeacon beacon;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + AD_TABLE + " WHERE " + AD_TABLE_UUID + " = '"
                        + ids.getUuid()
                        + "' AND " + AD_TABLE_MAJOR + " = '"
                        + ids.getMajor()
                        + "' AND " + AD_TABLE_MINOR + " = '"
                        + ids.getMinor() + "'",
                null);
        if (cursor.moveToFirst()) {
            beacon = getBeaconFromCursor(cursor);
        } else {
            beacon = new DBBeacon();
            beacon.setIds(ids);
            beacon.setNextShownContent(0);
        }
        return beacon.getNextShownContent();
    }

    private List<BeaconAdditionalContent> readAdditionalContent(String s) {
        List<BeaconAdditionalContent> contents = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + ADDITIONAL_TABLE + " WHERE " + ADDITIONAL_TABLE_BEACON_UUID + " = '" + s + "'", null);
        while (cursor.moveToNext()) {
            BeaconAdditionalContent content = getContentFromCursor(cursor);
            contents.add(content);
        }
        db.close();
        return contents;
    }

    private BeaconAdditionalContent getContentFromCursor(Cursor cursor) {
        BeaconAdditionalContent content = new BeaconAdditionalContent();
        content.setBeacon_id(cursor.getString(cursor.getColumnIndex(ADDITIONAL_TABLE_BEACON_ID)));
        content.setId(cursor.getString(cursor.getColumnIndex(ADDITIONAL_TABLE_ID)));
        content.setTitle(cursor.getString(cursor.getColumnIndex(ADDITIONAL_TABLE_TITLE)));
        content.setLink(cursor.getString(cursor.getColumnIndex(ADDITIONAL_TABLE_LINK)));
        content.setPicture(cursor.getString(cursor.getColumnIndex(ADDITIONAL_TABLE_PICTURE)));
        content.setDescription(cursor.getString(cursor.getColumnIndex(ADDITIONAL_TABLE_DESCRIPTION)));
        content.setAbsolutePicture(cursor.getString(cursor.getColumnIndex(ADDITIONAL_TABLE_ABSOLUTE_PPICTURE)));

        return content;
    }

    private void writeAdditionsToDb(DBBeacon dbBeacon) {
        SQLiteDatabase database = getWritableDatabase();

        List<BeaconAdditionalContent> additionalContent = dbBeacon.getAdditionalContent();
        if (additionalContent != null) {
            for (BeaconAdditionalContent content : additionalContent) {
                ContentValues cv = new ContentValues();
                cv.put(ADDITIONAL_TABLE_BEACON_UUID, dbBeacon.getIds().toString());
                cv.put(ADDITIONAL_TABLE_BEACON_ID, content.getBeacon_id());
                cv.put(ADDITIONAL_TABLE_ID, content.getId());
                cv.put(ADDITIONAL_TABLE_TITLE, content.getTitle());
                cv.put(ADDITIONAL_TABLE_LINK, content.getLink());
                cv.put(ADDITIONAL_TABLE_PICTURE, content.getHorizontal_picture());
                cv.put(ADDITIONAL_TABLE_DESCRIPTION, content.getDescription());
                cv.put(ADDITIONAL_TABLE_ABSOLUTE_PPICTURE, content.getAbsolutePicture());

                try {
                    database.insertWithOnConflict(ADDITIONAL_TABLE, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
                } catch (SQLiteConstraintException e) {
                    Log.d("DBHELPER", "Something wrong :" + e.getLocalizedMessage());
                }
            }
        }


    }

    public void updateNextShownContent(DBBeacon beacon) {
        int size = beacon.getAdditionalContent().size();
        int oldNextShownContent = beacon.getNextShownContent();
        oldNextShownContent++;
        if (oldNextShownContent > size) oldNextShownContent = 0;


        ContentValues cv = new ContentValues();
        SQLiteDatabase db = getWritableDatabase();
        cv.put(AD_TABLE_NEXT_CONTENT, oldNextShownContent);
        int updated = db.update(AD_TABLE, cv, AD_TABLE_UUID + " = ? and " + AD_TABLE_MAJOR + " = ? and " + AD_TABLE_MINOR + " = ?", new String[]{beacon.getIds().getUuid(), beacon.getIds().getMajor(), beacon.getIds().getMinor(),});

        int i = 2;

    }

    //TODO add delete to add beacon???
    public void deleteContent(BeaconIds ids) {
        SQLiteDatabase database = getWritableDatabase();
        database.delete(ADDITIONAL_TABLE, "id = ?", new String[]{ids.toString()});
    }

    //Return beacon by id
    private DBBeacon getBeaconByIds(BeaconIds ids) {
        DBBeacon beacon;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + AD_TABLE + " WHERE " + AD_TABLE_UUID + " = '"
                        + ids.getUuid()
                        + "' AND " + AD_TABLE_MAJOR + " = '"
                        + ids.getMajor()
                        + "' AND " + AD_TABLE_MINOR + " = '"
                        + ids.getMinor() + "'",
                null);
        if (cursor.moveToFirst()) {
            beacon = getBeaconFromCursor(cursor);
        } else {
            beacon = new DBBeacon();
            beacon.setIds(ids);
            beacon.setNextUpdateTime(0);
            //Need to download from internet
        }
        return beacon;
    }

    private DBBeacon changeToDbBeacon(BeaconIds ids) {
        DBBeacon beacon = new DBBeacon();
        beacon.setIds(ids);
        return beacon;
    }

    public void addOrUpdateBeacon(DBBeacon beacon) {

        SQLiteDatabase database = getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(AD_TABLE_UUID, beacon.getIds().getUuid());
        cv.put(AD_TABLE_MAJOR, beacon.getIds().getMajor());
        cv.put(AD_TABLE_MINOR, beacon.getIds().getMinor());
        cv.put(AD_TABLE_TITLE, beacon.getTitle());
        cv.put(AD_TABLE_PICTURE, beacon.getPicture());
        cv.put(AD_TABLE_LINK, beacon.getLink());
        cv.put(AD_TABLE_DESCRIPTION, beacon.getDescription());
        cv.put(AD_TABLE_GROUP_NAME, beacon.getGroupName());
        cv.put(AD_TABLE_GROUP_ID, beacon.getGroupToBind());
        cv.put(AD_TABLE_MAP_WIDTH, beacon.getMapWidth());
        cv.put(AD_TABLE_MAP_HEIGTH, beacon.getMapHeight());
        cv.put(AD_TABLE_BEACON_PIN_X, beacon.getBeaconPinX());
        cv.put(AD_TABLE_BEACON_PIN_Y, beacon.getBeaconPinY());
        cv.put(AD_TABLE_ABSOLUTE_MAP_FOLDER_URL, beacon.getAbsoluteMapFolderUrl());
        updateCacheTime();
        cv.put(AD_TABLE_NEXT_UPDATE_TIME, System.currentTimeMillis() + NOT_UPDATE_INTERVAL);
//        cv.put(AD_TABLE_NEXT_CONTENT, beacon.getNextShownContent());
        try {
            database.insertWithOnConflict("ads", null, cv, SQLiteDatabase.CONFLICT_REPLACE);
        } catch (SQLiteConstraintException e) {
            Log.d("DBHELPER", "Something wrong :" + e.getLocalizedMessage());
        }
        Log.d("DBHELPER", "Beacon added to local DB" + beacon.toString());
//        setNextUpdateTime(beacon.getIds(), System.currentTimeMillis() + NOT_UPDATE_INTERVAL);
    }

    public void updateCacheTime() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        NOT_UPDATE_INTERVAL = Long.valueOf(context.getResources().getString(R.string.update_time_period));
//        if (sharedPreferences.getBoolean("settings_key_cache_enabled", true)) {
//            NOT_UPDATE_INTERVAL = 10;
//        } else {
//            NOT_UPDATE_INTERVAL = Long.valueOf(sharedPreferences.getString("settings_key_cache_time", context.getResources().getString(R.string.update_time_period)));
//        }
    }

    //Set default data.
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("DATA", "--- onCreate database ---");

        db.execSQL("create table " + AD_TABLE + " ("
                + AD_TABLE_UUID + " text, "
                + AD_TABLE_MAJOR + " text, "
                + AD_TABLE_MINOR + " text, "
                + AD_TABLE_TITLE + " text, "
                + AD_TABLE_PICTURE + " text, "
                + AD_TABLE_LINK + " text, "
                + AD_TABLE_DESCRIPTION + " text, "
                + AD_TABLE_GROUP_ID + " text, "
                + AD_TABLE_GROUP_NAME + " text, "

                + AD_TABLE_MAP_WIDTH + " integer, "
                + AD_TABLE_MAP_HEIGTH + " integer, "
                + AD_TABLE_BEACON_PIN_X + " real, "
                + AD_TABLE_BEACON_PIN_Y + " real, "
                + AD_TABLE_ABSOLUTE_MAP_FOLDER_URL + " text, "
                + AD_TABLE_NEXT_CONTENT + " integer, "
                + AD_TABLE_FAVORITE + " integer, "
                + AD_TABLE_NEXT_SHOW_TIME + " long default 0, "
                + AD_TABLE_NEXT_UPDATE_TIME + " long default 0, "
                + "primary key (" + AD_TABLE_UUID + ", " + AD_TABLE_MAJOR + ", " + AD_TABLE_MINOR + ")"
                + ");");

        db.execSQL("create table " + ADDITIONAL_TABLE + " ("
                + ADDITIONAL_TABLE_BEACON_UUID + " text, "
                + ADDITIONAL_TABLE_ID + " text, "
                + ADDITIONAL_TABLE_BEACON_ID + " text, "
                + ADDITIONAL_TABLE_TITLE + " text, "
                + ADDITIONAL_TABLE_LINK + " text, "
                + ADDITIONAL_TABLE_DESCRIPTION + " text, "
                + ADDITIONAL_TABLE_PICTURE + " text, "
                + ADDITIONAL_TABLE_ABSOLUTE_PPICTURE + " text, "
                + "primary key (" + ADDITIONAL_TABLE_BEACON_UUID + ", " + ADDITIONAL_TABLE_ID + ")"
                + ");");


        db.execSQL("create table " + BLACK_LIST_TABLE + " ("
                + BLACK_LIST_TABLE_UUID + " text, "
                + BLACK_LIST_TABLE_MAJOR + " text, "
                + BLACK_LIST_TABLE_MINOR + " text, "
                + BLACK_LIST_TABLE_TITLE + " text, "
                + BLACK_LIST_TABLE_DESCRIPTION + " text, "
                + "primary key (" + BLACK_LIST_TABLE_UUID + ", " + BLACK_LIST_TABLE_MAJOR + ", " + BLACK_LIST_TABLE_MINOR + ")"
                + ");");


        db.execSQL("create table " + FAVORITES_TABLE + " ("
                + FAVORITES_TABLE_UUID + " text, "
                + FAVORITES_TABLE_MAJOR + " text, "
                + FAVORITES_TABLE_MINOR + " text, "
                + FAVORITES_TABLE_TITLE + " text, "
                + FAVORITES_TABLE_PICTURE + " text, "
                + FAVORITES_TABLE_LINK + " text, "
                + FAVORITES_TABLE_DESCRIPTION + " text, "
                + FAVORITES_TABLE_GROUP_ID + " text, "
                + FAVORITES_TABLE_GROUP_NAME + " text, "


                + FAVORITES_TABLE_BEACON_PIN_X + " real, "
                + FAVORITES_TABLE_BEACON_PIN_Y + " real, "
                + "primary key (" + FAVORITES_TABLE_UUID + ", " + FAVORITES_TABLE_MAJOR + ", " + FAVORITES_TABLE_MINOR + ")"
                + ");");

        db.execSQL("create table " + COORDINATES_TABLE + " ("
                + COORDINATES_TABLE_UUID + " text, "
                + COORDINATES_TABLE_MAJOR + " text, "
                + COORDINATES_TABLE_MINOR + " text, "
                + COORDINATES_TABLE_X + " real, "
                + COORDINATES_TABLE_Y + " real, "
                + "primary key (" + COORDINATES_TABLE_UUID + ", " + COORDINATES_TABLE_MAJOR + ", " + COORDINATES_TABLE_MINOR + ")"
                + ");");
    }

    //Calls when db version is changed.
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
       Log.d("DATA", "--- onUpgrade database ---");
        db.execSQL("DROP TABLE IF EXISTS " + AD_TABLE);
        if (oldVersion > 66) {
            db.execSQL("DROP TABLE IF EXISTS " + BLACK_LIST_TABLE);
        }
        if (oldVersion > 68) {
            db.execSQL("DROP TABLE IF EXISTS " + ADDITIONAL_TABLE);
        }
        if (oldVersion > 68) {
            db.execSQL("DROP TABLE IF EXISTS " + FAVORITES_TABLE);
        }
        if (oldVersion > 68) {
            db.execSQL("DROP TABLE IF EXISTS " + COORDINATES_TABLE);
        }
        onCreate(db);

    }

    public boolean isOnBlackList(BeaconIds ids) {
        boolean isOnBlackList = false;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + BLACK_LIST_TABLE + " WHERE " + BLACK_LIST_TABLE_UUID + "=\"" + ids.getUuid() + "\" AND " + BLACK_LIST_TABLE_MAJOR + "=\"" + ids.getMajor() + "\" AND " + BLACK_LIST_TABLE_MINOR + "=\"" + ids.getMinor() + "\"", null);
        if (cursor.getCount() > 0) {
            Log.d("DATA", "On blacklist " + ids.toString());
            isOnBlackList = true;
        }
        cursor.close();
        db.close();
        return isOnBlackList;
    }

    public List<DBBeacon> getBlackListFromDB() {
        ArrayList<DBBeacon> beaconsFromDB = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + BLACK_LIST_TABLE, null);
        while (cursor.moveToNext()) {
            DBBeacon beacon = new DBBeacon();
            BeaconIds ids = new BeaconIds(cursor.getString(cursor.getColumnIndex(BLACK_LIST_TABLE_UUID)),
                    cursor.getString(cursor.getColumnIndex(BLACK_LIST_TABLE_MAJOR)),
                    cursor.getString(cursor.getColumnIndex(BLACK_LIST_TABLE_MINOR)));
            beacon.setIds(ids);
            beacon.setTitle(cursor.getString(cursor.getColumnIndex(BLACK_LIST_TABLE_TITLE)));
            beacon.setDescription(cursor.getString(cursor.getColumnIndex(BLACK_LIST_TABLE_DESCRIPTION)));
            beaconsFromDB.add(beacon);
        }
        cursor.close();
        db.close();
        return beaconsFromDB;
    }

    public void removeFromBlackList(BeaconIds ids) {
        SQLiteDatabase database = getWritableDatabase();
        try {
            database.delete(BLACK_LIST_TABLE, BLACK_LIST_TABLE_UUID + "=? AND " + BLACK_LIST_TABLE_MAJOR + "=? AND " + BLACK_LIST_TABLE_MINOR + "=?", new String[]{ids.getUuid(), ids.getMajor(), ids.getMinor()});
        } catch (SQLiteConstraintException e) {
            Log.d("DBHELPER", "Something wrong :" + e.getLocalizedMessage());
        }
        Log.d("DBHELPER", "Beacon deleted from DB blacklist" + ids.toString());
    }

    public void addToBlackList(BeaconIds ids, String title, String description) {
        SQLiteDatabase database = getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(BLACK_LIST_TABLE_UUID, ids.getUuid());
        cv.put(BLACK_LIST_TABLE_MAJOR, ids.getMajor());
        cv.put(BLACK_LIST_TABLE_MINOR, ids.getMinor());
        cv.put(BLACK_LIST_TABLE_TITLE, title);
        cv.put(BLACK_LIST_TABLE_DESCRIPTION, description);

        try {
            database.insertWithOnConflict(BLACK_LIST_TABLE, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
        } catch (SQLiteConstraintException e) {
            Log.d("DBHELPER", "Something wrong :" + e.getLocalizedMessage());
        }
        Log.d("DBHELPER", "Beacon added to local DB blacklist" + ids.toString());
    }

    public boolean isFavorite(BeaconIds ids) {
        boolean isFavorite = false;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + FAVORITES_TABLE + " WHERE " + FAVORITES_TABLE_UUID + "=\"" + ids.getUuid() + "\" AND " + FAVORITES_TABLE_MAJOR + "=\"" + ids.getMajor() + "\" AND " + FAVORITES_TABLE_MINOR + "=\"" + ids.getMinor() + "\"", null);
        if (cursor.getCount() > 0) {
            Log.d("DATA", "On Favorites " + ids.toString());
            isFavorite = true;
        }
        cursor.close();
        db.close();
        return isFavorite;
    }

    public  void addToFavorites(BeaconIds ids) {
        SQLiteDatabase database = getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(FAVORITES_TABLE_UUID, ids.getUuid());
        cv.put(FAVORITES_TABLE_MAJOR, ids.getMajor());
        cv.put(FAVORITES_TABLE_MINOR, ids.getMinor());

        try {
            database.insertWithOnConflict(FAVORITES_TABLE, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
        } catch (SQLiteConstraintException e) {
            Log.d("DBHELPER", "Something wrong :" + e.getLocalizedMessage());
        }
        Log.d("DBHELPER", "Beacon added to local DB favorites" + ids.toString());
    }

    public void removeFromFavorites(BeaconIds ids) {
        SQLiteDatabase database = getWritableDatabase();
        try {
            database.delete(FAVORITES_TABLE, FAVORITES_TABLE_UUID + "=? AND " + FAVORITES_TABLE_MAJOR + "=? AND " + FAVORITES_TABLE_MINOR + "=?", new String[]{ids.getUuid(), ids.getMajor(), ids.getMinor()});
        } catch (SQLiteConstraintException e) {
            Log.d("DBHELPER", "Something wrong :" + e.getLocalizedMessage());
        }
        Log.d("DBHELPER", "Beacon deleted from DB favorites" + ids.toString());
    }

    public List<BeaconIds>  getFavorites () {
        ArrayList<BeaconIds> favoritesIds = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + FAVORITES_TABLE, null);

        while (cursor.moveToNext()) {
            BeaconIds ids = new BeaconIds(cursor.getString(cursor.getColumnIndex(FAVORITES_TABLE_UUID)),
                    cursor.getString(cursor.getColumnIndex(FAVORITES_TABLE_MAJOR)),
                    cursor.getString(cursor.getColumnIndex(FAVORITES_TABLE_MINOR)));

            favoritesIds.add(ids);
        }
        cursor.close();
        db.close();
        return favoritesIds;
    }

    public void setCoordinates(BeaconIds ids, double x, double y) {
        SQLiteDatabase database = getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(COORDINATES_TABLE_UUID, ids.getUuid());
        cv.put(COORDINATES_TABLE_MAJOR, ids.getMajor());
        cv.put(COORDINATES_TABLE_MINOR, ids.getMinor());
        cv.put(COORDINATES_TABLE_X, x);
        cv.put(COORDINATES_TABLE_Y, y);
        try {
            database.insertWithOnConflict(COORDINATES_TABLE, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
        } catch (SQLiteConstraintException e) {
            Log.d("DBHELPER", "Something wrong :" + e.getLocalizedMessage());
        }
        Log.d("DBHELPER", "Beacon added to local DB coordinates" + ids.toString());
    }

    public Coordinates getCoordinates(BeaconIds ids) {
        Coordinates coordinates = new Coordinates();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + COORDINATES_TABLE + " WHERE " + COORDINATES_TABLE_UUID + "=\"" + ids.getUuid() + "\" AND " + COORDINATES_TABLE_MAJOR + "=\"" + ids.getMajor() + "\" AND " + COORDINATES_TABLE_MINOR + "=\"" + ids.getMinor() + "\"", null);
        while (cursor.moveToNext()) {
            Log.d("DATA", "On Coordinets " + ids.toString());
            coordinates.setX(cursor.getFloat(cursor.getColumnIndex(COORDINATES_TABLE_X)));
            coordinates.setY(cursor.getFloat(cursor.getColumnIndex(COORDINATES_TABLE_Y)));
        }
        cursor.close();
        db.close();
        return coordinates;
    }
}
