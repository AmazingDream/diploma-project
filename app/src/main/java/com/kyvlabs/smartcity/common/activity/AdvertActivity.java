package com.kyvlabs.smartcity.common.activity;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.kyvlabs.smartcity.services.LocateService;
import com.kyvlabs.smartcity.smartcity.R;
import com.pkmmte.view.CircularImageView;

import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdvertActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.navigation)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advert);
        ButterKnife.bind(this);

        activity = this;
        initToolbar();
        initNavigationView();

        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();

        AdvertFragment advertFragment = new AdvertFragment();
        advertFragment.setArguments(getIntent().getExtras());

        if (savedInstanceState == null) {
            fragmentManager.beginTransaction()
                    .add(R.id.container, advertFragment)
                    .commit();
        }
    }
    private void initToolbar() {
        toolbar.setTitle(R.string.app_name);
    }

    private void initNavigationView() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.key_action_bar_vew_navigation_open, R.string.key_action_bar_vew_navigation_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                drawerLayout.closeDrawers();
                switch (item.getItemId()) {
   /*                 case R.id.navigation_action_beacons: {
                        Intent intent = new Intent(activity, MainActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                    }
                    case R.id.navigation_action_nearby: {
                        activity.stopService(new Intent(activity, LocateService.class));
                        Intent intent = new Intent(activity, NearbyActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                    }
                    case R.id.navigation_action_locate: {
                        activity.startService(new Intent(activity, LocateService.class));
                        Toast.makeText(activity, R.string.scan_started, Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                    }
                    case R.id.navigation_action_stopLocating: {
                        activity.stopService(new Intent(activity, LocateService.class));
                        Toast.makeText(activity, R.string.scan_stopped, Toast.LENGTH_SHORT).show();
                        break;
                    }
                    case R.id.navigation_action_favorite: {
                        Intent intent = new Intent(activity, FavoriteActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                    }*/
                    case R.id.navigation_action_logout: {
                        SharedPreferences preferences = getSharedPreferences(getString(R.string.saved_auth_key), 0);
                        preferences.edit().remove(getString(R.string.saved_auth_key)).commit();
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                        finish();
                        break;
                    }
                    case R.id.navigation_action_settings: {
                        Intent intent = new Intent(AdvertActivity.this, SettingsActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_action_about: {
                        Intent intent = new Intent(AdvertActivity.this, AboutActivity.class);
                        startActivity(intent);
                        break;
                    }
                }
                return true;
            }
        });

        View headerView = LayoutInflater.from(this).inflate(R.layout.navigation_drawer_header, navigationView, false);
        navigationView.addHeaderView(headerView);

        CircularImageView faceImageView = ButterKnife.findById(headerView, R.id.brrr_logo);

        try {
            InputStream ims = getAssets().open("brrr.jpg");
            Drawable d = Drawable.createFromStream(ims, null);
            faceImageView.setImageDrawable(d);
        } catch (IOException e) {
            e.printStackTrace();
        }

        TextView nameTextView = ButterKnife.findById(headerView, R.id.name);
        nameTextView.setText(R.string.default_drawer_text);

        TextView emailTextView = ButterKnife.findById(headerView, R.id.email);
        emailTextView.setText(R.string.default_drawer_mail);
    }

    private void setCheckedNavigationDrawerMenuItem(int navigationId) {
        navigationView.setCheckedItem(navigationId);
    }

}
