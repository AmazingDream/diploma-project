package com.kyvlabs.smartcity.common.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.kyvlabs.smartcity.Application;
import com.kyvlabs.smartcity.common.adapter.CSRicyclerAdapter;
import com.kyvlabs.smartcity.data.BeaconIds;
import com.kyvlabs.smartcity.data.DBBeacon;
import com.kyvlabs.smartcity.data.DBHelper;
import com.kyvlabs.smartcity.services.LocateService;
import com.kyvlabs.smartcity.smartcity.R;
import com.pkmmte.view.CircularImageView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;


import com.kyvlabs.smartcity.smartcity.R;

import org.altbeacon.beacon.BeaconManager;

public class FavoriteActivity extends AppCompatActivity {

    protected static final String LOG_TAG = "FavoriteActivity";


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.navigation)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    Activity activity;
  //  MyTask mt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);

        activity = this;
        ButterKnife.bind(this);


        initToolbar();
        initNavigationView();


        FavoriteActivity.PlaceholderFragment listFragment = new FavoriteActivity.PlaceholderFragment();
        getFragmentManager().beginTransaction()
                .add(R.id.container, listFragment)
                .commit();
        getFavorites();

     /*   mt = new MyTask();
        mt.execute();
*/
    }

    private static void getFavorites() {
        DBHelper dbHelper = new DBHelper();
        PlaceholderFragment.setBeacons(new ArrayList<>(dbHelper.getFavorites()));
        dbHelper.close();
    }
    private void initToolbar() {
        toolbar.setTitle(R.string.app_name);
    }

    private void initNavigationView() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.key_action_bar_vew_navigation_open, R.string.key_action_bar_vew_navigation_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        setCheckedNavigationDrawerMenuItem(R.id.navigation_action_favorite);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                drawerLayout.closeDrawers();
                switch (item.getItemId()) {
                    case R.id.navigation_action_beacons: {
                        Intent intent = new Intent(activity, MainActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                    }
                    case R.id.navigation_action_nearby: {
                        activity.stopService(new Intent(activity, LocateService.class));
                        Intent intent = new Intent(activity, NearbyActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                    }
                    case R.id.navigation_action_locate: {
                        activity.startService(new Intent(activity, LocateService.class));
                        Toast.makeText(activity, R.string.scan_started, Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                    }
                    case R.id.navigation_action_stopLocating: {
                        activity.stopService(new Intent(activity, LocateService.class));
                        Toast.makeText(activity, R.string.scan_stopped, Toast.LENGTH_SHORT).show();
                        break;
                    }
                    case R.id.navigation_action_favorite: {
                        Intent intent = new Intent(activity, FavoriteActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                    }
                    case R.id.navigation_action_logout: {
                        SharedPreferences preferences = getSharedPreferences(getString(R.string.saved_auth_key), 0);
                        preferences.edit().remove(getString(R.string.saved_auth_key)).commit();
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        LoginManager.getInstance().logOut();
                        finish();
                        break;
                    }
                    case R.id.navigation_action_settings: {
                        Intent intent = new Intent(FavoriteActivity.this, SettingsActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_action_about: {
                        Intent intent = new Intent(FavoriteActivity.this, AboutActivity.class);
                        startActivity(intent);
                        break;
                    }
                }
                return true;
            }
        });

        View headerView = LayoutInflater.from(this).inflate(R.layout.navigation_drawer_header, navigationView, false);
        navigationView.addHeaderView(headerView);

        CircularImageView faceImageView = ButterKnife.findById(headerView, R.id.brrr_logo);

        try {
            InputStream ims = getAssets().open("brrr.jpg");
            Drawable d = Drawable.createFromStream(ims, null);
            faceImageView.setImageDrawable(d);
        } catch (IOException e) {
            e.printStackTrace();
        }

        TextView nameTextView = ButterKnife.findById(headerView, R.id.name);
        nameTextView.setText(R.string.default_drawer_text);

        TextView emailTextView = ButterKnife.findById(headerView, R.id.email);
        emailTextView.setText(R.string.default_drawer_mail);
    }

    private void setCheckedNavigationDrawerMenuItem(int navigationId) {
        navigationView.setCheckedItem(navigationId);
    }

    public static class PlaceholderFragment extends Fragment {

        private static List<DBBeacon> beaconsFromDB = new ArrayList<>();
        // private static AdsListViewAdapter beaconsAdapter;
        private static CSRicyclerAdapter beaconsAdapter;
        private static ListView adsListView;
        private static View rootView;
        private static final int LAYOUT = R.layout.fragment_city_stream;
        @BindView(R.id.city_stream_recycler_view)
        RecyclerView mRecyclerView;
        @BindView(R.id.city_stream_swipe_refresh_layout)
        SwipeRefreshLayout mSwipeRefreshLayout;


        private static Handler UIHandler = new Handler(Looper.getMainLooper());

        public PlaceholderFragment() {
        }

        //set beacons collection to list after reading from db
        public static void setBeacons(Collection<BeaconIds> beacons) {
            getBeaconsFromDB(beacons);
        }

        //After scanning reading founded beacons list from dB.
        private static void getBeaconsFromDB(Collection<BeaconIds> beaconCollection) {

            SharedPreferences userDetails = Application.getAppContext().getSharedPreferences(Application.getAppContext().getString(R.string.saved_auth_key), Context.MODE_PRIVATE);
            String auth = userDetails.getString(Application.getAppContext().getString(R.string.saved_auth_key), "");

            DBHelper dbHelper = new DBHelper();
            dbHelper.getBeaconsByIds(beaconCollection, auth)
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(new Action0() {
                        @Override
                        public void call() {
                            clearAdapter();
                        }
                    })
                    .subscribe(
                            new Action1<DBBeacon>() {
                                @Override
                                public void call(final DBBeacon beacon) {
                                    addBeaconToAdapter(beacon);
                                }
                            },
                            new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {
                                    Log.d("Talk on error", "", throwable);
                                }
                            });

            dbHelper.close();

        }

        private static void clearAdapter() {
            runOnUi(new Runnable() {
                @Override
                public void run() {
                    beaconsFromDB.clear();
                    beaconsAdapter.notifyDataSetChanged();
                }
            });
        }

        private static void addBeaconToAdapter(final DBBeacon beacon) {
            runOnUi(new Runnable() {
                @Override
                public void run() {
                    Set<DBBeacon> set = new HashSet<>(beaconsFromDB);
                    if (!set.contains(beacon)) {
                        beaconsFromDB.add(beacon);
                        beaconsAdapter.notifyDataSetChanged();
                    }
                }
            });
        }

        private static void runOnUi(Runnable runnable) {
            UIHandler.post(runnable);
        }

        @Override
        public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
            switch (requestCode) {
                case 0: {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.length <= 0
                            || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(getActivity(), "Sorry but we need this permission", Toast.LENGTH_LONG).show();
                        getActivity().finish();
                    }
                }

            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    Toast.makeText(getActivity(), "Sorry but we need this permission", Toast.LENGTH_LONG).show();
                    getActivity().finish();
                } else {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                            0);
                }
            }

            rootView = inflater.inflate(LAYOUT, container, false);
            ButterKnife.bind(this, rootView);
            //adsListView = (ListView) rootView.findViewById(R.id.talk_beacons_list);
            beaconsAdapter = new CSRicyclerAdapter(beaconsFromDB);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            //adsListView.setAdapter(beaconsAdapter);
            mRecyclerView.setAdapter(beaconsAdapter);
            mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getFavorites();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    },2000);
                }
            });
         /*   adsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getActivity().getApplicationContext(), AdvertActivity.class);
                    DBBeacon dbBeacon = (DBBeacon) parent.getAdapter().getItem(position);

                    int nextShownContent = dbBeacon.getNextShownContent();

                    if (nextShownContent <= 0) {
                        intent.putExtra(DataKeys.AD_TITLE_KEY, dbBeacon.getTitle());
                        intent.putExtra(DataKeys.AD_PICTURE_KEY, dbBeacon.getPicture());
                        intent.putExtra(DataKeys.AD_LINK_KEY, dbBeacon.getLink());
                        intent.putExtra(DataKeys.AD_DESCRIPTION_KEY, dbBeacon.getDescription());
                    } else {
                        BeaconAdditionalContent beaconAdditionalContent = dbBeacon.getAdditionalContent().get(nextShownContent - 1);
                        intent.putExtra(DataKeys.AD_TITLE_KEY, beaconAdditionalContent.getTitle());
                        intent.putExtra(DataKeys.AD_PICTURE_KEY, beaconAdditionalContent.getAbsolutePicture());
                        intent.putExtra(DataKeys.AD_LINK_KEY, beaconAdditionalContent.getLink());
                        intent.putExtra(DataKeys.AD_DESCRIPTION_KEY, beaconAdditionalContent.getDescription());
                    }

                    intent.putExtra(DataKeys.BEACON_IDS, dbBeacon.getIds());
                    intent.putExtra(DataKeys.AD_GROUP_NAME, dbBeacon.getGroupName());
                    intent.putExtra(DataKeys.AD_TABLE_MAP_WIDTH, dbBeacon.getMapWidth());
                    intent.putExtra(DataKeys.AD_TABLE_MAP_HEIGTH, dbBeacon.getMapHeight());
                    intent.putExtra(DataKeys.AD_TABLE_BEACON_PIN_X, dbBeacon.getBeaconPinX());
                    intent.putExtra(DataKeys.AD_TABLE_BEACON_PIN_Y, dbBeacon.getBeaconPinY());
                    intent.putExtra(DataKeys.AD_TABLE_ABSOLUTE_MAP_FOLDER_URL, dbBeacon.getAbsoluteMapFolderUrl());

                    new DBHelper().updateNextShownContent(dbBeacon);

                    startActivity(intent);
                    getActivity().finish();
                }
            });*/
            return rootView;
        }
     /*   private void verifyBluetooth() {
            try {
                if (!BeaconManager.getInstanceForApplication(getActivity()).checkAvailability()) {
                    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    if (!mBluetoothAdapter.isEnabled()) {
                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBtIntent, BT_REQUEST);
                    }
                }
            } catch (RuntimeException e) {
                showBTLENotSupportedMessage();
            }
        }
        private void showBTLENotSupportedMessage() {
            DialogHelper.showOkDialog(getActivity(), R.string.bluetooth_activation_request_title, R.string.bluetooth_activation_request_message);
        }*/

        @Override
        public void onStart() {
            super.onStart();
            clearAdapter();
        }

        @Override
        public void onResume() {
            super.onResume();
            clearAdapter();
        }

    }

}
