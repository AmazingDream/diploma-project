package com.kyvlabs.smartcity.smartcity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kyvlabs.smartcity.data.BeaconIds;
import com.kyvlabs.smartcity.data.Coordinates;
import com.kyvlabs.smartcity.data.DBHelper;
import com.kyvlabs.smartcity.views.HtmlTagHandler;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleApiClient mGoogleApiClient;
    private GoogleMap mMap;
    private static final int PERMISSIONS_REQUEST_ACCESS_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Do other setup activities here too, as described elsewhere in this tutorial.


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_LOCATION);
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

        }
        if (!isLocationServiceEnabled()) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Seems Like location service is off. Enable it to show your location.")
                    .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                        }
                    }).setNegativeButton("NO THANKS", null).create().show();
        }
        mMap.setMyLocationEnabled(true);

        Intent intent = getIntent();
        // Add a marker  and move the camera
        // LatLng beaconPos = new LatLng(intent.getDoubleExtra("x",47.8176353), intent.getDoubleExtra("y",35.1840204));

        DBHelper dbHelper = new DBHelper();
      /*  Coordinates coordinates1 = dbHelper.getCoordinates(new BeaconIds("ebefd083-70a2-47c8-9837-e7b5634df524","1","1"));
        LatLng beaconPos1 = new LatLng(coordinates1.getX(), coordinates1.getY());
        Coordinates coordinates2 = dbHelper.getCoordinates(new BeaconIds("ebefd083-70a2-47c8-9837-e7b5634df524","1","5"));
        LatLng beaconPos2 = new LatLng(coordinates2.getX(), coordinates2.getY());
        Coordinates coordinates3 = dbHelper.getCoordinates(new BeaconIds("ebefd083-70a2-47c8-9837-e7b5634df524","1","4"));
        LatLng beaconPos3 = new LatLng(coordinates3.getX(), coordinates3.getY());*/
      Coordinates coordinates1 = dbHelper.getCoordinates((BeaconIds) getIntent().getParcelableExtra("ids"));
      LatLng beaconPos = new LatLng(coordinates1.getX(), coordinates1.getY());
        mMap.addMarker(new MarkerOptions().position(beaconPos).title(intent.getStringExtra("title")).snippet(String.valueOf(Html.fromHtml(intent.getStringExtra("description"), null, new HtmlTagHandler()))));
     //   mMap.addMarker(new MarkerOptions().position(beaconPos2));//.title(intent.getStringExtra("title")).snippet(String.valueOf(Html.fromHtml(intent.getStringExtra("description"), null, new HtmlTagHandler()))));
      //  mMap.addMarker(new MarkerOptions().position(beaconPos3));//.title(intent.getStringExtra("title")).snippet(String.valueOf(Html.fromHtml(intent.getStringExtra("description"), null, new HtmlTagHandler()))));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(beaconPos, 15));
        dbHelper.close();

    }
    public boolean isLocationServiceEnabled(){
        LocationManager locationManager = null;
        boolean gps_enabled= false,network_enabled = false;

        if(locationManager ==null)
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        try{
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }catch(Exception ex){
            //do nothing...
        }

        try{
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }catch(Exception ex){
            //do nothing...
        }

        return gps_enabled || network_enabled;

    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Location permission granted", Toast.LENGTH_LONG);
                }
            }
        }
    }

}
