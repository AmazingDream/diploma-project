package com.kyvlabs.smartcity;

import android.app.Application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.kyvlabs.smartcity.smartcity.R;


/**
 * Это подкласс {@link Application}, с помощью которого приложению передаются общие объекты,
 * например {@link Tracker}.
 */

public class AnalyticsApplication extends Application {
    private Tracker mTracker;

    /**
     * Получает счетчик {@link Tracker}, используемый по умолчанию для этого приложения {@link Application}.
     * @return tracker
     */

    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // Чтобы включить ведение журнала отладки, используйте adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.layout.activity_main); // Ввести суда PROPERTY_ID
        }
        return mTracker;
    }
}
