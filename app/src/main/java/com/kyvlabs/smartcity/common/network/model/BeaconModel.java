package com.kyvlabs.smartcity.common.network.model;

import java.util.List;

public class BeaconModel {
    private String status;
    private String title;
    private String description;
    private String absolutePicture;
    private String link;
    private String uuid;
    private String minor;
    private String major;
    private String groupToBind;
    private String groupName;
    private String absoluteMapUrl;
    private int mapWidth;
    private int mapHeight;
    private double beaconPinX;
    private double beaconPinY;
    private int favorite;
    private List<BeaconAdditionalContent> content;


    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getMinor() {
        return minor;
    }

    public void setMinor(String minor) {
        this.minor = minor;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "BeaconModel{" +
                "ids= '" + uuid + ":" + major + ":" + minor + '\'' +
                "status='" + status + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", absolutePicture='" + absolutePicture + '\'' +
                ", link='" + link + '\'' +
                ", groupId='" + groupToBind + '\'' +
                ", groupName='" + groupName + '\'' +
                ", absoluteMapUrl='" + absoluteMapUrl + '\'' +
                ", mapWidth='" + mapWidth + '\'' +
                ", mapHeight='" + mapHeight + '\'' +
                ", beaconPinX='" + beaconPinX + '\'' +
                ", beaconPinY='" + beaconPinY + '\'' +
                ", contentSize=" + content.size() +
                '}';
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAbsolutePicture() {
        return absolutePicture;
    }

    public void setAbsolutePicture(String absolutePicture) {
        this.absolutePicture = absolutePicture;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getGroupToBind() {
        return groupToBind;
    }

    public void setGroupToBind(String groupToBind) {
        this.groupToBind = groupToBind;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getAbsoluteMapUrl() {
        return absoluteMapUrl;
    }

    public void setAbsoluteMapUrl(String absoluteMapUrl) {
        this.absoluteMapUrl = absoluteMapUrl;
    }

    public int getMapWidth() {
        return mapWidth;
    }

    public void setMapWidth(int mapWidth) {
        this.mapWidth = mapWidth;
    }

    public int getMapHeight() {
        return mapHeight;
    }

    public void setMapHeight(int mapHeight) {
        this.mapHeight = mapHeight;
    }

    public double getBeaconPinX() {
        return beaconPinX;
    }

    public void setBeaconPinX(double beaconPinX) {
        this.beaconPinX = beaconPinX;
    }

    public double getBeaconPinY() {
        return beaconPinY;
    }

    public void setBeaconPinY(double beaconPinY) {
        this.beaconPinY = beaconPinY;
    }

    public List<BeaconAdditionalContent> getContent() {
        return content;
    }

    public void setContent(List<BeaconAdditionalContent> content) {
        this.content = content;
    }

    public int getFavorite() { return favorite; }

    public void setFavorite(int favorite) { this.favorite = favorite; }
}